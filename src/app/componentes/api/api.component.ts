import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.css']
})
export class ApiComponent implements OnInit {


  mensajes: any;


  constructor(private dataService: DataService) { }

  ngOnInit(): void {

    this.mensajes = this.dataService.getData();
    

  }

  onClick(){
    console.log(this.mensajes);
    console.log("Estas clickeando un elemento");
  }

}
