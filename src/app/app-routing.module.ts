import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {Routes, RouterModule} from '@angular/router';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { FormularioComponent } from './componentes/formulario/formulario.component';
import { ApiComponent } from './componentes/api/api.component';
import { PostsModule } from './componentes/posts/posts.module';


const routes: Routes =[
{
  path: 'inicio',
  component:InicioComponent
},
{
path:'formulario',
component:FormularioComponent
},
  {
    path:'posts',
    loadChildren:'./componentes/posts/posts.module#PostsModule'
    },
  {
    path:'**',
    redirectTo:'inicio'

  }
];


@NgModule({
  declarations: [],
  imports: [
  RouterModule.forRoot(routes)
  ],
  exports:[
    RouterModule,
    PostsModule
  ]
})
export class AppRoutingModule { }
