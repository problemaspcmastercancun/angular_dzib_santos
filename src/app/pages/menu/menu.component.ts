import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  rutas = [
    {
name:'Página 1',
path:'/Inicio'
    },
    {
   name:'Formulario',
   path:'/formulario'
    }, 
     {
      name:'Api',
      path:'/posts'
    }
  ];



  constructor() { }

  ngOnInit(): void {
  }

}
