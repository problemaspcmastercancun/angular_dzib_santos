import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { FormularioComponent } from './componentes/formulario/formulario.component';
import { ApiComponent } from './componentes/api/api.component';
import { AppRoutingModule } from './app-routing.module';
import { MenuComponent } from './pages/menu/menu.component';
import { PagesModule } from './componentes/pages.module';
import {HttpClientModule} from '@angular/common/http';

import { from } from 'rxjs';
@NgModule({
  declarations: [
    AppComponent,
  //  InicioComponent,
   // FormularioComponent,
    //ApiComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PagesModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
